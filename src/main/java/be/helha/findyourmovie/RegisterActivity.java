package be.helha.findyourmovie;

import android.app.Notification;
import android.app.NotificationManager;
import android.graphics.Color;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import be.helha.findyourmovie.exceptions.DuplicateException;
import be.helha.findyourmovie.exceptions.MailAlreadyPresentException;
import be.helha.findyourmovie.exceptions.MailUncorrectException;
import be.helha.findyourmovie.exceptions.UsernameAlreadyPresent;
import be.helha.findyourmovie.model.User;
import be.helha.findyourmovie.model.UserManagement;
import be.helha.findyourmovie.registerFragment.RegisterOneFragment;
import be.helha.findyourmovie.registerFragment.RegisterTwoFragment;

public class RegisterActivity extends AppCompatActivity implements RegisterOneFragment.Listener, RegisterTwoFragment.Listener {
    private String username;
    private String password;
    private String mail;
    private String firstName;
    private String lastName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fl_register_container, RegisterOneFragment.newInstance(this))
                .commit();
    }

    @Override
    public void nextStep() {
        username = ((EditText) findViewById(R.id.et_register_username)).getText().toString();
        password = ((EditText) findViewById(R.id.et_register_Password)).getText().toString();
        mail = ((EditText) findViewById(R.id.et_register_mail)).getText().toString();

        TextView tvUsernameError = (TextView) findViewById(R.id.tv_register_error_username);
        TextView tvMailError = (TextView) findViewById(R.id.tv_register_error_mail);

        try{
            if(!UserManagement.getInstance().containsUsername(username.toLowerCase()) && !UserManagement.getInstance().containsMail(mail.toLowerCase()) && User.verifyMail(mail)){
                tvUsernameError.setVisibility(View.GONE);
                tvMailError.setVisibility(View.GONE);
                tvMailError.setText(R.string.error_message_for_mail_adress);

                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fl_register_container, RegisterTwoFragment.newInstance(this))
                        .addToBackStack(null)
                        .commit();
            }
        }catch (MailAlreadyPresentException mail){
            tvUsernameError.setVisibility(View.GONE);
            tvMailError.setText(R.string.this_mail_is_already_use);
            tvMailError.setVisibility(View.VISIBLE);
        } catch (MailUncorrectException e) {
            tvUsernameError.setVisibility(View.GONE);
            tvMailError.setText(R.string.this_mail_is_uncorrect);
            tvMailError.setVisibility(View.VISIBLE);
        } catch (UsernameAlreadyPresent usernameAlreadyPresent) {
            tvMailError.setVisibility(View.GONE);
            tvUsernameError.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void finishSubscrive() {
        firstName = ((EditText) findViewById(R.id.et_register_first_name)).getText().toString();
        lastName = ((EditText) findViewById(R.id.et_register_last_name)).getText().toString();

        User user = registerUser();
        showNotification(user);

        FragmentManager manager = getSupportFragmentManager();
        for (Fragment fragment : manager.getFragments()) {
            manager.beginTransaction()
                    .remove(fragment)
                    .commit();
        }
        for (int i = 0; i < manager.getBackStackEntryCount(); i++) {
            manager.popBackStack();
        }
        finish();
    }

    public User registerUser() {
        User user = new User(username.toLowerCase(),password,mail.toLowerCase(),firstName,lastName);
        try {
            UserManagement.getInstance().addUser(user);
        } catch (DuplicateException e) {}
        return user;
    }

    private void showNotification(User user) {
        Notification notification = new NotificationCompat.Builder(this)
                .setSmallIcon(android.R.drawable.ic_dialog_info)
                .setContentTitle("Welcome "+ user.getUsername() +" !")
                .setContentText("Come when you want!")
                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                .setVibrate(new long[]{200, 100, 200, 100, 1000})
                .setLights(Color.argb(1, 0, 0, 1), 200, 100)
                .build();

        NotificationManager notificationManager
                = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(1, notification);
    }
}
