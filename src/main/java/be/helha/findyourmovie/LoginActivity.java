package be.helha.findyourmovie;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import be.helha.findyourmovie.exceptions.DuplicateException;
import be.helha.findyourmovie.exceptions.UsernameAlreadyPresent;
import be.helha.findyourmovie.model.User;
import be.helha.findyourmovie.model.UserManagement;

public class LoginActivity extends AppCompatActivity {

    private EditText etUsername;
    private EditText etPassword;
    private CheckBox cbRemember;
    private Button btnLogin;
    private Button btnReset;
    private Button btnRegister;
    private SharedPreferences preferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        try {
            UserManagement.getInstance().addUser(new User("Paradow","helha","paradow@helha.be","Joffrey","Desantoine"));
        } catch (DuplicateException e) {}

        etUsername = (EditText) findViewById(R.id.et_login_username);
        etPassword = (EditText) findViewById(R.id.et_login_password);
        cbRemember = (CheckBox) findViewById(R.id.cb_login_remember);
        btnLogin = (Button) findViewById(R.id.btn_login_login);
        btnReset = (Button) findViewById(R.id.btn_login_reset);
        btnRegister = (Button) findViewById(R.id.btn_login_register);

        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        boolean isRemembered = preferences.getBoolean("remember", false);
        if (isRemembered) {
            etUsername.setText(preferences.getString("username", ""));
            etPassword.setText(preferences.getString("password", ""));
        }
        cbRemember.setChecked(isRemembered);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    UserManagement.getInstance().containsUsername(etUsername.getText().toString().toLowerCase());
                    Toast.makeText(LoginActivity.this, R.string.this_username_does_not_exist, Toast.LENGTH_SHORT).show();
                } catch (UsernameAlreadyPresent usernameAlreadyPresent) {
                    User u = UserManagement.getInstance().getUserByUsername(etUsername.getText().toString());
                    if(u.getPassword().equals(etPassword.getText().toString())){
                        Toast.makeText(LoginActivity.this, "I'm going to menu!", Toast.LENGTH_SHORT).show();

                        SharedPreferences.Editor editor = preferences.edit();

                        if (cbRemember.isChecked()) {
                            editor.putString("username", etUsername.getText().toString());
                            editor.putString("password", etPassword.getText().toString());
                            editor.putBoolean("remember", true);
                        } else {
                            editor.remove("username");
                            editor.remove("password");
                            editor.putBoolean("remember", false);
                        }
                        editor.apply();
                    }else{
                        Toast.makeText(LoginActivity.this, "Password uncorrect!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetChamp();
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent toIntentRegister = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(toIntentRegister);
            }
        });

    }

    public void resetChamp(){
        etUsername.setText("");
        etPassword.setText("");
    }
}
