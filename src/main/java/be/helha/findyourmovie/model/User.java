package be.helha.findyourmovie.model;

import java.util.Date;
import java.util.regex.Pattern;

import be.helha.findyourmovie.exceptions.MailUncorrectException;

/**
 * Created by Desan on 06/10/2017.
 */

public class User {
    private String username;
    private String password;
    private String mail;
    private String firstName;
    private String lastName;

    public User(String username, String password, String mail, String firstName, String lastName) {
        this.username = username;
        this.password = password;
        this.mail = mail;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    // TODO Create the HASHCODE
    public String getPassword() {
        return password;
    }

    public String getMail() {
        return mail;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setMail(String mail)  {
        this.mail = mail;
    }

    public User clone(){
        return new User(username,password,mail,firstName,lastName);
    }

    public static boolean verifyMail(String mail) throws MailUncorrectException {
        if(Pattern.matches("\\b[\\w.%-]+@[-.\\w]+\\.[A-Za-z]{2,4}\\b", mail)){
            return true;
        }
        throw new MailUncorrectException();
    }

    @Override
    public boolean equals(Object o){
        if(o instanceof User){
            User u = (User) o;
            return this.username.equals(((User) o).username) && this.mail.equals(u.mail);
        }
        return false;
    }
}