package be.helha.findyourmovie.model;

import java.util.ArrayList;
import java.util.List;

import be.helha.findyourmovie.exceptions.DuplicateException;
import be.helha.findyourmovie.exceptions.MailAlreadyPresentException;
import be.helha.findyourmovie.exceptions.UnpresentException;
import be.helha.findyourmovie.exceptions.UsernameAlreadyPresent;

public class UserManagement {

	private List<User> users;
	private static UserManagement instance = null;
	
	/**
	 * private constructor that initialises the list of users 
	 */
	private UserManagement(){
		users = new ArrayList<User>();
	}
	
	/*
	 * gets one instanc of the usermanagement class
	 */
	public static UserManagement getInstance(){
		if(instance == null){
			instance = new UserManagement();
		}
		return instance;
	}
	/*
	 * adds a user to the list of users
	 */
	public void addUser(User user)throws DuplicateException{
		if(!users.contains(user))
			users.add(user);
		else
			throw new DuplicateException();
	}
	
	/*
	 * deletes a user from the list
	 */
	public void deleteUser(User user)throws UnpresentException{
		if(users.remove(user)){}
		else{
			throw new UnpresentException();
		}
	}
	/**
	 * update a user in the list 
	 * @param user user to update 
	 * @param newUser new user that replaces the old one 
	 * @throws UnpresentException occurs when the to updated user isn't found 
	 */
	public void updateUser(User user, User newUser) throws UnpresentException{
		int i = this.searchUser(user);
		users.set(i, newUser);
		
	}
	
	/**
	 * get a user at a specific index 
	 * @param i index of the user 
	 * @return the user at the specific index
	 */
	public User getUser(int i){
		return users.get(i).clone();
	}
	
	/**
	 * searches the index of a specific user 
	 * @param user to find the index from 
	 * @return the index of the specific user 
	 * @throws UnpresentException occurs when the user isn't in the list
	 */
	public int searchUser(User user) throws UnpresentException{
		int i = users.indexOf(user);
		if(i == -1){
			throw new UnpresentException();
		}
		return i;
	}
	
	/**
	 * return a string value of the user attributes 
	 */
	public String toString(){
		String s = "";
		for(User u: users){
			s += u + "\n";
		}
		return  s;
	}

	/**
	 * get a clone of the list of users 
	 * @return returns the cloned list of users 
	 */
	public List<User> getListUser() {
		List<User> listU = new ArrayList<User>();
		
		for(User u : users){
			listU.add(u.clone());
		}
		return listU;
	}
	
	/**
	 * set the list of users to a new list of users
	 * @param users
	 */
	public void setlistUser(List<User> users) {
		this.users = users;
	}
	
	/**
	 * get the size of the list
	 * @return
	 */
	public int getSize(){
		return users.size();
	}

	public boolean containsMail(String mail) throws MailAlreadyPresentException {
		for(User u : users){
			if(u.getMail().equals(mail)){
				throw new MailAlreadyPresentException();
			}
		}

		return false;
	}

	public boolean containsUsername(String username) throws UsernameAlreadyPresent {
		for(User u : users){
			if(u.getUsername().equals(username)){
				throw new UsernameAlreadyPresent();
			}
		}
		return false;
	}

	public User getUserByUsername(String username){
		for(User u: users){
			if(u.getUsername().equals(username)){
				return u.clone();
			}
		}
		return null;
	}
}
