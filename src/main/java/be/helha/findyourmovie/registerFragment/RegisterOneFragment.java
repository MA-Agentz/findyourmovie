package be.helha.findyourmovie.registerFragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import be.helha.findyourmovie.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterOneFragment extends Fragment {

    public interface Listener{
        void nextStep();
    }

    private Listener listener;
    private Button btnNext;

    public RegisterOneFragment() {}

    public static RegisterOneFragment newInstance(Listener listener) {
        RegisterOneFragment fragment = new RegisterOneFragment();
        fragment.setListener(listener);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = (View) inflater.inflate(R.layout.fragment_register_one, container, false);

        btnNext = (Button) v.findViewById(R.id.btn_register_totwostep);

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.nextStep();
            }
        });

        return v;
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }
}
