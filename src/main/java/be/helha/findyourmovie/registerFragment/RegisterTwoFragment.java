package be.helha.findyourmovie.registerFragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.Date;

import be.helha.findyourmovie.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterTwoFragment extends Fragment {

    public interface Listener{
        void finishSubscrive();
    }

    private Listener listener;
    private Button btnFinish;

    public RegisterTwoFragment() {}

    public static RegisterTwoFragment newInstance(Listener listener) {
        RegisterTwoFragment fragment = new RegisterTwoFragment();
        fragment.setListener(listener);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = (View) inflater.inflate(R.layout.fragment_register_two, container, false);
        btnFinish = (Button) v.findViewById(R.id.btn_register_finish);
        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener != null)
                    listener.finishSubscrive();
            }
        });

        return v;
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }
}

